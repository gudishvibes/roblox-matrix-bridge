local HttpService = game:GetService("HttpService")
local TextService = game:GetService("TextService")

HOMESERVER_URL = "https://server.bob.com"
TOKEN = "INSERT_TOKEN"
ROOM = "!internal_room_id:bob.com" -- DO NOT use a room alias.
USERNAME = "@username:bob.com" -- to avoid looping like a midek copescript battle
IDENTIFIER = string.sub(game.JobId, -5)

screen = workspace.screen.SurfaceGui.Frame
screen.username.Text = USERNAME
screen.server_id.Text = "Server ID: "..IDENTIFIER
screen.room_id.Text = "Internal room: "..ROOM

-- relay messages from roblox
function send_message(username, text, id)
	local response
	local txn_id = "m"..math.random() -- transaction ids are like nonces, i think
	local text = TextService:FilterStringAsync(text, id):GetNonChatStringForBroadcastAsync()
	local message = {
		msgtype = "m.text",
		body = "<"..IDENTIFIER.."><"..username.."> "..text,
		["cf.bestgirl.roblox.server"] = tostring(game.JobId),
		["cf.bestgirl.roblox.sender"] = username,
		["cf.bestgirl.roblox.message"] = text
	}
	local request_data = {
		Url = HOMESERVER_URL.."/_matrix/client/r0/rooms/"..HttpService:UrlEncode(ROOM).."/send/m.room.message/"..txn_id,
		Method = "PUT",
		Headers = {
			["Content-type"] = "application/json",
			["Authorization"] = "Bearer "..TOKEN
		},
		Body = HttpService:JSONEncode(message)
	}
	pcall(function ()
		response = HttpService:RequestAsync(request_data)
	end)
end

-- make a sync request and get the batch token
-- in element android, this is the thing of nightmares
function initial_sync()
	local response
	local newlabel
	local request_data = {
		Url = HOMESERVER_URL.."/_matrix/client/r0/sync",
		Method = "GET",
		Headers = {
			["Authorization"] = "Bearer "..TOKEN
		}
	}
	pcall(function()
		response = HttpService:JSONDecode(HttpService:RequestAsync(request_data)["Body"])
	end)
	if response then
		for number, state_event in ipairs(response["rooms"]["join"][ROOM]["state"]["events"]) do
			if state_event["type"] == "cf.bestgirl.roblox.experiment1" then -- feel free to remove this, idk if i care about this
				newlabel = workspace.experiment1.SurfaceGui.ScrollingFrame.TextLabel:Clone()
				newlabel.Name = state_event["state_key"]
				newlabel.Text = state_event["state_key"].." while in "..state_event["content"]["server"]
				newlabel.Parent = workspace.experiment1.SurfaceGui.ScrollingFrame
			end
		end
		print("(initial!) next batch token is", response["next_batch"], response["rooms"])
		return response["next_batch"]
	else
		return ""
	end
end

function sync_and_send(batch_token)
	local response
	local timeout = 500 -- in milliseconds
	local request_data = {
		-- TODO: find more elegant way to get URL parameters in
		Url = HOMESERVER_URL.."/_matrix/client/r0/sync?since="..batch_token.."&timeout="..timeout,
		Method = "GET",
		Headers = {
			["Authorization"] = "Bearer "..TOKEN
		}
	}
	pcall(function()
		response = HttpService:JSONDecode(HttpService:RequestAsync(request_data)["Body"])
	end)
	if response then
		print(response)
		print("batch token used was", batch_token)
		print("next batch token is", response["next_batch"])
		if response["rooms"] then  -- if anything was new, this would exist
			for number, event in ipairs(response["rooms"]["join"][ROOM]["timeline"]["events"]) do
				if event["type"] == "m.room.message" then
					-- is it a message from another roblox server?
					if event["sender"] == USERNAME then
						print(event["content"]["body"]["cf.bestgirl.roblox.server"], "|", game.JobId)
						if event["content"]["body"]["cf.bestgirl.roblox.server"] ~= game.JobId then 
							local content = event["content"]
							print("MESSAGE FROM SERVER", content["cf.bestgirl.roblox.server"])
							send_to_chat(content["cf.bestgirl.roblox.sender"], content["cf.bestgirl.roblox.message"]) -- TODO: nickname resolution?
						end
					end
					-- is it a message from matrix?
					if event["sender"] ~= USERNAME then
						send_to_chat(
							event["sender"],
							--TextService:FilterStringAsync(event["content"]["body"], 1):GetNonChatStringForBroadcastAsync()
							event["content"]["body"]
						)
					end
				end
				if event["type"] == "cf.bestgirl.experiment1" then
					local newlabel = workspace.experiment1.SurfaceGui.ScrollingFrame.TextLabel:Clone()
					newlabel.Name = event["state_key"]
					newlabel.Text = event["state_key"].." | "..event["content"]["server"]
					newlabel.Parent = workspace.experiment1.SurfaceGui.ScrollingFrame
				end
				print(event)
			end
		end
		return response["next_batch"]
	else
		return ""
	end
end

function send_to_chat(username, text)
	local filteredtext
	for o,player in ipairs(game.Players:GetChildren()) do 
		filteredtext = TextService:FilterStringAsync(text, player.UserId):GetChatForUserAsync(player.UserId)
		workspace.matrix_message_event:FireClient(player, username, filteredtext)
	end
end

game.Players.PlayerAdded:Connect(function(player)
	player.Chatted:Connect(function(msg)
		send_message(player.Name, msg, player.UserId)
	end)
end)

next_batch = sync_and_send(initial_sync())
while true do
	next_batch = sync_and_send(next_batch)
	print(next_batch)
end