# roblox-matrix-bridge

The union of something pooprietary (Minetest would have been more useful) and something based. A bridge for one ROBLOX game to Matrix. It also unifies the chats of multiple servers.

## How to use

1. Paste the contents of `your_bridge.lua` into a `Script`. As long as the server runs it, it doesn't matter where. Configure `HOMESERVER_URL`, `TOKEN`, `ROOM` and `USERNAME` accordingly.
2. `clientside.lua` should be in a `LocalScript` in `game.StarterPlayer.StarterPlayerScripts`.
3. Create a `RemoteEvent` called `matrix_message_event` in `Workspace`. If you have any reason to change the location or name, `^F` is your friend (Ctrl+F).
4. Profit. Depending on your homeserver, it may be a little slow. 

## Special thanks

I thank [Semisol](https://matrix.to/#/@semisol:semisol.dev) and [AwesomeSheep48](@awesomesheep48:matrix.awesomesheep48.me) for helping me test on real servers! 